import axios from "axios";


const KEY = "AIzaSyBP9Smx2Csr3JbsDEphnKZn-ZC977JD-HI";

export default axios.create({
    baseURL: "https://www.googleapis.com/youtube/v3",
    params: {
        part: "snippet",
        maxResult: 5,
        key: KEY
    }

});